module.exports = {
  env: {
    browser: true,
    es2021: true
  },
  extends: 'standard-with-typescript',
  overrides: [
  ],
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
    project: ['./tsconfig.json']
  },
  rules: {
    '@typescript-eslint/semi': ['error', 'always'],
    '@typescript-eslint/space-before-function-paren': ['error', 'never'],
    'indent': ['error', 2, { SwitchCase: 1 }],
    '@typescript-eslint/member-delimiter-style': [
      'warn', 
      {
        multiline: {
          delimiter: 'semi'
        }, 
        singleline: {
          delimiter: 'semi'}
        }
      ],
  }
}
