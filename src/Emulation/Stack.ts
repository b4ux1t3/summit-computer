export class Stack {
  private readonly members: number[] = [];

  public push(value: number): void {
    this.members.push(value);
  }

  public pop(): number {
    const num = this.members.pop();
    return num === undefined ? 0 : num;
  }

  public dumpContents(): number[] {
    return [...this.members];
  }
}
