import { Stack } from './Stack';
import { AddressingCode, DebugFrame, Instruction, ProgramLine } from './Types';

export class Computer {
  private readonly randomAccessMemory: number[] = [];

  private currentInstruction: Instruction = 0;

  private programCounter: number = 0;
  private equalityFlag = false;
  private readonly registers: number[] = [];
  private readonly stack: Stack = new Stack();

  private halted = true;

  constructor(memorySize: number, private readonly debuggerContext: number = 5, romContents: number[] = []) {
    this.randomAccessMemory = romContents;

    if (this.randomAccessMemory.length < memorySize) {
      const slackToFill = memorySize - this.randomAccessMemory.length;
      for (let i = 0; i < slackToFill; i++) this.randomAccessMemory.push(0);
    }

    for (let i = 0; i < 3; i++) this.registers.push(0);
  }

  public start(): DebugFrame {
    this.halted = false;
    return this.debugFrame();
  }

  public pause(): void {
    console.log('I\'ve been paused!');
  }

  public cycle(): DebugFrame {
    if (this.halted) return this.debugFrame();
    this.currentInstruction = this.randomAccessMemory[this.programCounter++] as Instruction;
    this.execute();
    return this.debugFrame();
  }

  public halt(): void {
    this.halted = true;
  }

  private peekMemory(address: number, context: number = 0): ProgramLine[] {
    if (address >= this.randomAccessMemory.length) throw new Error('Memory address out of range.');
    if (context === 0) return [{ lineNumber: address, value: this.randomAccessMemory[address] }];
    const highAddress = address + context;
    if (context > address) {
      return this.randomAccessMemory
        .slice(0, highAddress)
        .map((val, i) => {
          return { lineNumber: i, value: val };
        });
    }
    const lowAddress = address - context;
    if (highAddress >= this.randomAccessMemory.length) {
      return this.randomAccessMemory
        .slice(lowAddress)
        .map((val, i) => {
          return { lineNumber: i + lowAddress, value: val };
        });
    }
    return this.randomAccessMemory
      .slice(lowAddress, highAddress)
      .map((val, i) => {
        return { lineNumber: i + lowAddress, value: val };
      });
  }

  private debugFrame(): DebugFrame {
    const newFrame: DebugFrame = {
      programList: this.peekMemory(this.programCounter, this.debuggerContext),
      equalityFlag: this.equalityFlag,
      programCounter: this.programCounter,
      registers: this.registers,
      stack: this.stack.dumpContents(),
      halted: this.halted
    };

    return newFrame;
  }

  private absoluteOperand(): number {
    const addr = this.randomAccessMemory[this.programCounter++];
    return this.randomAccessMemory[addr];
  }

  private offsetOperand(): number {
    const addr = this.programCounter + this.randomAccessMemory[this.programCounter++];
    return this.randomAccessMemory[addr];
  }

  private absoluteRegisterOperand(register: number): number {
    const addr = this.registers[register];
    return this.randomAccessMemory[addr];
  }

  private offsetRegisterOperand(register: number): number {
    const addr = this.registers[register] + this.programCounter;
    return this.randomAccessMemory[addr];
  }

  private absoluteStackOperand(): number {
    const addr = this.stack.pop();
    return this.randomAccessMemory[addr];
  }

  private offsetStackOperand(): number {
    const addr = this.stack.pop() + this.programCounter;
    return this.randomAccessMemory[addr];
  }

  private getNextOperand(): number {
    switch (this.randomAccessMemory[this.programCounter++] as AddressingCode) {
      case 0x0:
        return this.randomAccessMemory[this.programCounter++];
      case 0x1:
        return this.absoluteOperand();
      case 0x2:
        return this.offsetOperand();
      case 0x3:
        return this.registers[0];
      case 0x4:
        return this.registers[1];
      case 0x5:
        return this.registers[2];
      case 0x6:
        return this.absoluteRegisterOperand(0);
      case 0x7:
        return this.absoluteRegisterOperand(1);
      case 0x8:
        return this.absoluteRegisterOperand(2);
      case 0x9:
        return this.offsetRegisterOperand(0);
      case 0xa:
        return this.offsetRegisterOperand(1);
      case 0xb:
        return this.offsetRegisterOperand(2);
      case 0xc:
        return this.stack.pop();
      case 0xd:
        return this.absoluteStackOperand();
      case 0xe:
        return this.offsetStackOperand();
      case 0xf:
        return this.programCounter;
    }
  }

  private movImmediate(): void {
    this.randomAccessMemory[this.programCounter++] = this.getNextOperand();
  }

  private movAbsolute(): void {
    const addr = this.randomAccessMemory[this.programCounter++];
    this.randomAccessMemory[addr] = this.getNextOperand();
  }

  private movOffset(): void {
    const addr = this.programCounter + this.randomAccessMemory[this.programCounter++];
    this.randomAccessMemory[addr] = this.getNextOperand();
  }

  private movRegister(register: number): void {
    this.registers[register] = this.getNextOperand();
  }

  private movRegisterAddress(register: number): void {
    const addr = this.registers[register];
    this.randomAccessMemory[addr] = this.getNextOperand();
  }

  private movRegisterOffset(register: number): void {
    const addr = this.registers[register] + this.programCounter;
    this.randomAccessMemory[addr] = this.getNextOperand();
  }

  private movStack(): void {
    this.stack.push(this.getNextOperand());
  }

  private movStackAddress(): void {
    const addr = this.stack.pop();
    this.randomAccessMemory[addr] = this.getNextOperand();
  }

  private movStackOffset(): void {
    const addr = this.stack.pop() + this.programCounter;
    this.randomAccessMemory[addr] = this.getNextOperand();
  }

  private movProgramCounter(): void {
    this.programCounter = this.getNextOperand();
  }

  private mov(): void {
    switch (this.randomAccessMemory[this.programCounter++]) {
      case 0x0:
        return this.movImmediate();
      case 0x1:
        return this.movAbsolute();
      case 0x2:
        return this.movOffset();
      case 0x3:
        return this.movRegister(0);
      case 0x4:
        return this.movRegister(1);
      case 0x5:
        return this.movRegister(2);
      case 0x6:
        return this.movRegisterAddress(0);
      case 0x7:
        return this.movRegisterAddress(1);
      case 0x8:
        return this.movRegisterAddress(2);
      case 0x9:
        return this.movRegisterOffset(0);
      case 0xa:
        return this.movRegisterOffset(1);
      case 0xb:
        return this.movRegisterOffset(2);
      case 0xc:
        return this.movStack();
      case 0xd:
        return this.movStackAddress();
      case 0xe:
        return this.movStackOffset();
      case 0xf:
        return this.movProgramCounter();
    }
  }

  private incImmediate(): void {
    this.randomAccessMemory[this.programCounter++]++;
  }

  private incAbsolute(): void {
    const addr = this.randomAccessMemory[this.programCounter++];
    this.randomAccessMemory[addr]++;
  }

  private incOffset(): void {
    const addr = this.programCounter + this.randomAccessMemory[this.programCounter++];
    this.randomAccessMemory[addr]++;
  }

  private incRegister(register: number): void {
    this.registers[register]++;
  }

  private incRegisterAddress(register: number): void {
    const addr = this.registers[register];
    this.randomAccessMemory[addr]++;
  }

  private incRegisterOffset(register: number): void {
    const addr = this.programCounter + this.registers[register];
    this.randomAccessMemory[addr]++;
  }

  private incStack(): void {
    const value = this.stack.pop() + 1;
    this.stack.push(value);
  }

  private incStackAddress(): void {
    const addr = this.stack.pop();
    this.randomAccessMemory[addr]++;
  }

  private incStackOffset(): void {
    const addr = this.programCounter + this.stack.pop();
    this.randomAccessMemory[addr]++;
  }

  private incProgramCounter(): void {
    this.programCounter++;
  }

  private inc(): void {
    switch (this.randomAccessMemory[this.programCounter++]) {
      case 0x0:
        return this.incImmediate();
      case 0x1:
        return this.incAbsolute();
      case 0x2:
        return this.incOffset();
      case 0x3:
        return this.incRegister(0);
      case 0x4:
        return this.incRegister(1);
      case 0x5:
        return this.incRegister(2);
      case 0x6:
        return this.incRegisterAddress(0);
      case 0x7:
        return this.incRegisterAddress(1);
      case 0x8:
        return this.incRegisterAddress(2);
      case 0x9:
        return this.incRegisterOffset(0);
      case 0xa:
        return this.incRegisterOffset(1);
      case 0xb:
        return this.incRegisterOffset(2);
      case 0xc:
        return this.incStack();
      case 0xd:
        return this.incStackAddress();
      case 0xe:
        return this.incStackOffset();
      case 0xf:
        return this.incProgramCounter();
    }
  }

  private decImmediate(): void {
    this.randomAccessMemory[this.programCounter++]--;
  }

  private decAbsolute(): void {
    const addr = this.randomAccessMemory[this.programCounter++];
    this.randomAccessMemory[addr]--;
  }

  private decOffset(): void {
    const addr = this.programCounter + this.randomAccessMemory[this.programCounter++];
    this.randomAccessMemory[addr]--;
  }

  private decRegister(register: number): void {
    this.registers[register]--;
  }

  private decRegisterAddress(register: number): void {
    const addr = this.registers[register];
    this.randomAccessMemory[addr]--;
  }

  private decRegisterOffset(register: number): void {
    const addr = this.programCounter + this.registers[register];
    this.randomAccessMemory[addr]--;
  }

  private decStack(): void {
    const value = this.stack.pop() - 1;
    this.stack.push(value);
  }

  private decStackAddress(): void {
    const addr = this.stack.pop();
    this.randomAccessMemory[addr]--;
  }

  private decStackOffset(): void {
    const addr = this.programCounter + this.stack.pop();
    this.randomAccessMemory[addr]--;
  }

  private decProgramCounter(): void {
    this.programCounter++;
  }

  private dec(): void {
    switch (this.randomAccessMemory[this.programCounter++]) {
      case 0x0:
        return this.decImmediate();
      case 0x1:
        return this.decAbsolute();
      case 0x2:
        return this.decOffset();
      case 0x3:
        return this.decRegister(0);
      case 0x4:
        return this.decRegister(1);
      case 0x5:
        return this.decRegister(2);
      case 0x6:
        return this.decRegisterAddress(0);
      case 0x7:
        return this.decRegisterAddress(1);
      case 0x8:
        return this.decRegisterAddress(2);
      case 0x9:
        return this.decRegisterOffset(0);
      case 0xa:
        return this.decRegisterOffset(1);
      case 0xb:
        return this.decRegisterOffset(2);
      case 0xc:
        return this.decStack();
      case 0xd:
        return this.decStackAddress();
      case 0xe:
        return this.decStackOffset();
      case 0xf:
        return this.decProgramCounter();
    }
  }

  private andImmediate(): void {
    const addr = this.programCounter++;
    const nextOp = this.getNextOperand();
    this.randomAccessMemory[addr] &= nextOp;
  }

  private andAbsolute(): void {
    const addr = this.randomAccessMemory[this.programCounter++];
    const nextOp = this.getNextOperand();
    this.randomAccessMemory[addr] &= nextOp;
  }

  private andOffset(): void {
    const addr = this.programCounter + this.randomAccessMemory[this.programCounter++];
    const nextOp = this.getNextOperand();
    this.randomAccessMemory[addr] &= nextOp;
  }

  private andRegister(register: number): void {
    const nextOp = this.getNextOperand();
    this.registers[register] &= nextOp;
  }

  private andRegisterAddress(register: number): void {
    const addr = this.randomAccessMemory[this.registers[register]];
    const nextOp = this.getNextOperand();
    this.randomAccessMemory[addr] &= nextOp;
  }

  private andRegisterOffset(register: number): void {
    const addr = this.programCounter + this.randomAccessMemory[this.registers[register]];
    const nextOp = this.getNextOperand();
    this.randomAccessMemory[addr] &= nextOp;
  }

  private andStack(): void {
    const value = this.stack.pop();
    const nextOp = this.getNextOperand();
    this.stack.push(value & nextOp);
  }

  private andStackAddress(): void {
    const addr = this.stack.pop();
    const nextOp = this.getNextOperand();
    this.randomAccessMemory[addr] &= nextOp;
  }

  private andStackOffset(): void {
    const addr = this.programCounter + this.stack.pop();
    const nextOp = this.getNextOperand();
    this.randomAccessMemory[addr] &= nextOp;
  }

  private andProgramCounter(): void {
    this.programCounter &= this.getNextOperand();
  }

  private and(): void {
    switch (this.randomAccessMemory[this.programCounter++]) {
      case 0x0:
        return this.andImmediate();
      case 0x1:
        return this.andAbsolute();
      case 0x2:
        return this.andOffset();
      case 0x3:
        return this.andRegister(0);
      case 0x4:
        return this.andRegister(1);
      case 0x5:
        return this.andRegister(2);
      case 0x6:
        return this.andRegisterAddress(0);
      case 0x7:
        return this.andRegisterAddress(1);
      case 0x8:
        return this.andRegisterAddress(2);
      case 0x9:
        return this.andRegisterOffset(0);
      case 0xa:
        return this.andRegisterOffset(1);
      case 0xb:
        return this.andRegisterOffset(2);
      case 0xc:
        return this.andStack();
      case 0xd:
        return this.andStackAddress();
      case 0xe:
        return this.andStackOffset();
      case 0xf:
        return this.andProgramCounter();
    }
  }

  private borImmediate(): void {
    const addr = this.programCounter++;
    const nextOp = this.getNextOperand();
    this.randomAccessMemory[addr] |= nextOp;
  }

  private borAbsolute(): void {
    const addr = this.randomAccessMemory[this.programCounter++];
    const nextOp = this.getNextOperand();
    this.randomAccessMemory[addr] |= nextOp;
  }

  private borOffset(): void {
    const addr = this.programCounter + this.randomAccessMemory[this.programCounter++];
    const nextOp = this.getNextOperand();
    this.randomAccessMemory[addr] |= nextOp;
  }

  private borRegister(register: number): void {
    const nextOp = this.getNextOperand();
    this.registers[register] |= nextOp;
  }

  private borRegisterAddress(register: number): void {
    const addr = this.randomAccessMemory[this.registers[register]];
    const nextOp = this.getNextOperand();
    this.randomAccessMemory[addr] |= nextOp;
  }

  private borRegisterOffset(register: number): void {
    const addr = this.programCounter + this.randomAccessMemory[this.registers[register]];
    const nextOp = this.getNextOperand();
    this.randomAccessMemory[addr] |= nextOp;
  }

  private borStack(): void {
    const value = this.stack.pop();
    const nextOp = this.getNextOperand();
    this.stack.push(value | nextOp);
  }

  private borStackAddress(): void {
    const addr = this.stack.pop();
    const nextOp = this.getNextOperand();
    this.randomAccessMemory[addr] |= nextOp;
  }

  private borStackOffset(): void {
    const addr = this.programCounter + this.stack.pop();
    const nextOp = this.getNextOperand();
    this.randomAccessMemory[addr] |= nextOp;
  }

  private borProgramCounter(): void {
    this.programCounter |= this.getNextOperand();
  }

  private bor(): void {
    switch (this.randomAccessMemory[this.programCounter++]) {
      case 0x0:
        return this.borImmediate();
      case 0x1:
        return this.borAbsolute();
      case 0x2:
        return this.borOffset();
      case 0x3:
        return this.borRegister(0);
      case 0x4:
        return this.borRegister(1);
      case 0x5:
        return this.borRegister(2);
      case 0x6:
        return this.borRegisterAddress(0);
      case 0x7:
        return this.borRegisterAddress(1);
      case 0x8:
        return this.borRegisterAddress(2);
      case 0x9:
        return this.borRegisterOffset(0);
      case 0xa:
        return this.borRegisterOffset(1);
      case 0xb:
        return this.borRegisterOffset(2);
      case 0xc:
        return this.borStack();
      case 0xd:
        return this.borStackAddress();
      case 0xe:
        return this.borStackOffset();
      case 0xf:
        return this.borProgramCounter();
    }
  }

  private xorImmediate(): void {
    const addr = this.programCounter++;
    const nextOp = this.getNextOperand();
    this.randomAccessMemory[addr] ^= nextOp;
  }

  private xorAbsolute(): void {
    const addr = this.randomAccessMemory[this.programCounter++];
    const nextOp = this.getNextOperand();
    this.randomAccessMemory[addr] ^= nextOp;
  }

  private xorOffset(): void {
    const addr = this.programCounter + this.randomAccessMemory[this.programCounter++];
    const nextOp = this.getNextOperand();
    this.randomAccessMemory[addr] ^= nextOp;
  }

  private xorRegister(register: number): void {
    const nextOp = this.getNextOperand();
    this.registers[register] ^= nextOp;
  }

  private xorRegisterAddress(register: number): void {
    const addr = this.randomAccessMemory[this.registers[register]];
    const nextOp = this.getNextOperand();
    this.randomAccessMemory[addr] ^= nextOp;
  }

  private xorRegisterOffset(register: number): void {
    const addr = this.programCounter + this.randomAccessMemory[this.registers[register]];
    const nextOp = this.getNextOperand();
    this.randomAccessMemory[addr] ^= nextOp;
  }

  private xorStack(): void {
    const value = this.stack.pop();
    const nextOp = this.getNextOperand();
    this.stack.push(value ^ nextOp);
  }

  private xorStackAddress(): void {
    const addr = this.stack.pop();
    const nextOp = this.getNextOperand();
    this.randomAccessMemory[addr] ^= nextOp;
  }

  private xorStackOffset(): void {
    const addr = this.programCounter + this.stack.pop();
    const nextOp = this.getNextOperand();
    this.randomAccessMemory[addr] ^= nextOp;
  }

  private xorProgramCounter(): void {
    this.programCounter ^= this.getNextOperand();
  }

  private xor(): void {
    switch (this.randomAccessMemory[this.programCounter++]) {
      case 0x0:
        return this.xorImmediate();
      case 0x1:
        return this.xorAbsolute();
      case 0x2:
        return this.xorOffset();
      case 0x3:
        return this.xorRegister(0);
      case 0x4:
        return this.xorRegister(1);
      case 0x5:
        return this.xorRegister(2);
      case 0x6:
        return this.xorRegisterAddress(0);
      case 0x7:
        return this.xorRegisterAddress(1);
      case 0x8:
        return this.xorRegisterAddress(2);
      case 0x9:
        return this.xorRegisterOffset(0);
      case 0xa:
        return this.xorRegisterOffset(1);
      case 0xb:
        return this.xorRegisterOffset(2);
      case 0xc:
        return this.xorStack();
      case 0xd:
        return this.xorStackAddress();
      case 0xe:
        return this.xorStackOffset();
      case 0xf:
        return this.xorProgramCounter();
    }
  }

  private notImmediate(): void {
    const addr = this.programCounter++;
    this.randomAccessMemory[addr] = ~this.randomAccessMemory[addr];
  }

  private notAbsolute(): void {
    const addr = this.randomAccessMemory[this.programCounter++];
    this.randomAccessMemory[addr] = ~this.randomAccessMemory[addr];
  }

  private notOffset(): void {
    const addr = this.programCounter + this.randomAccessMemory[this.programCounter++];
    this.randomAccessMemory[addr] = ~this.randomAccessMemory[addr];
  }

  private notRegister(register: number): void {
    this.registers[register] = ~this.registers[register];
  }

  private notRegisterAddress(register: number): void {
    const addr = this.registers[register];
    this.randomAccessMemory[addr] = ~this.randomAccessMemory[addr];
  }

  private notRegisterOffset(register: number): void {
    const addr = this.programCounter + this.registers[register];
    this.randomAccessMemory[addr] = ~this.randomAccessMemory[addr];
  }

  private notStack(): void {
    const value = ~this.stack.pop();
    this.stack.push(value);
  }

  private notStackAddress(): void {
    const addr = this.stack.pop();
    this.randomAccessMemory[addr] = ~this.randomAccessMemory[addr];
  }

  private notStackOffset(): void {
    const addr = this.programCounter = this.stack.pop();
    this.randomAccessMemory[addr] = ~this.randomAccessMemory[addr];
  }

  private notProgramCounter(): void {
    this.programCounter = ~this.programCounter;
  }

  private not(): void {
    switch (this.randomAccessMemory[this.programCounter++]) {
      case 0x0:
        return this.notImmediate();
      case 0x1:
        return this.notAbsolute();
      case 0x2:
        return this.notOffset();
      case 0x3:
        return this.notRegister(0);
      case 0x4:
        return this.notRegister(1);
      case 0x5:
        return this.notRegister(2);
      case 0x6:
        return this.notRegisterAddress(0);
      case 0x7:
        return this.notRegisterAddress(1);
      case 0x8:
        return this.notRegisterAddress(2);
      case 0x9:
        return this.notRegisterOffset(0);
      case 0xa:
        return this.notRegisterOffset(1);
      case 0xb:
        return this.notRegisterOffset(2);
      case 0xc:
        return this.notStack();
      case 0xd:
        return this.notStackAddress();
      case 0xe:
        return this.notStackOffset();
      case 0xf:
        return this.notProgramCounter();
    }
  }

  private teqImmediate(): void {
    const value = this.randomAccessMemory[this.programCounter++];
    this.equalityFlag = value === this.getNextOperand();
  }

  private teqAbsolute(): void {
    const addr = this.randomAccessMemory[this.programCounter++];
    const value = this.randomAccessMemory[addr];
    this.equalityFlag = value === this.getNextOperand();
  }

  private teqOffset(): void {
    const addr = this.programCounter + this.randomAccessMemory[this.programCounter++];
    const value = this.randomAccessMemory[addr];
    this.equalityFlag = value === this.getNextOperand();
  }

  private teqRegister(register: number): void {
    const value = this.registers[register];
    this.equalityFlag = value === this.getNextOperand();
  }

  private teqRegisterAddress(register: number): void {
    const addr = this.registers[register];
    const value = this.randomAccessMemory[addr];
    this.equalityFlag = value === this.getNextOperand();
  }

  private teqRegisterOffset(register: number): void {
    const addr = this.programCounter + this.registers[register];
    const value = this.randomAccessMemory[addr];
    this.equalityFlag = value === this.getNextOperand();
  }

  private teqStack(): void {
    const value = this.stack.pop();
    this.equalityFlag = value === this.getNextOperand();
  }

  private teqStackAddress(): void {
    const addr = this.stack.pop();
    const value = this.randomAccessMemory[addr];
    this.equalityFlag = value === this.getNextOperand();
  }

  private teqStackOffset(): void {
    const addr = this.programCounter + this.stack.pop();
    const value = this.randomAccessMemory[addr];
    this.equalityFlag = value === this.getNextOperand();
  }

  private teqProgramCounter(): void {
    this.equalityFlag = this.programCounter === this.getNextOperand();
  }

  private teq(): void {
    switch (this.randomAccessMemory[this.programCounter++]) {
      case 0x0:
        return this.teqImmediate();
      case 0x1:
        return this.teqAbsolute();
      case 0x2:
        return this.teqOffset();
      case 0x3:
        return this.teqRegister(0);
      case 0x4:
        return this.teqRegister(1);
      case 0x5:
        return this.teqRegister(2);
      case 0x6:
        return this.teqRegisterAddress(0);
      case 0x7:
        return this.teqRegisterAddress(1);
      case 0x8:
        return this.teqRegisterAddress(2);
      case 0x9:
        return this.teqRegisterOffset(0);
      case 0xa:
        return this.teqRegisterOffset(1);
      case 0xb:
        return this.teqRegisterOffset(2);
      case 0xc:
        return this.teqStack();
      case 0xd:
        return this.teqStackAddress();
      case 0xe:
        return this.teqStackOffset();
      case 0xf:
        return this.teqProgramCounter();
    }
  }

  private jmpImmediate(): void {
    const addr = this.randomAccessMemory[this.programCounter];
    this.programCounter = addr;
  }

  private jmpAbsolute(): void {
    const addr = this.randomAccessMemory[this.programCounter];
    this.programCounter = this.randomAccessMemory[addr];
  }

  private jmpOffset(): void {
    const addr = this.randomAccessMemory[this.programCounter];
    this.programCounter += this.randomAccessMemory[addr];
  }

  private jmpRegister(register: number): void {
    this.programCounter = this.registers[register];
  }

  private jmpRegisterAddress(register: number): void {
    this.programCounter = this.registers[register];
  }

  private jmpRegisterOffset(register: number): void {
    this.programCounter += this.registers[register];
  }

  private jmpStack(): void {
    this.programCounter = this.stack.pop();
  }

  private jmpStackAddress(): void {
    this.programCounter = this.stack.pop();
  }

  private jmpStackOffset(): void {
    this.programCounter += this.stack.pop();
  }

  private jmpProgramCounter(): void {

  }

  private jmp(): void {
    switch (this.randomAccessMemory[this.programCounter++]) {
      case 0x0:
        return this.jmpImmediate();
      case 0x1:
        return this.jmpAbsolute();
      case 0x2:
        return this.jmpOffset();
      case 0x3:
        return this.jmpRegister(0);
      case 0x4:
        return this.jmpRegister(1);
      case 0x5:
        return this.jmpRegister(2);
      case 0x6:
        return this.jmpRegisterAddress(0);
      case 0x7:
        return this.jmpRegisterAddress(1);
      case 0x8:
        return this.jmpRegisterAddress(2);
      case 0x9:
        return this.jmpRegisterOffset(0);
      case 0xa:
        return this.jmpRegisterOffset(1);
      case 0xb:
        return this.jmpRegisterOffset(2);
      case 0xc:
        return this.jmpStack();
      case 0xd:
        return this.jmpStackAddress();
      case 0xe:
        return this.jmpStackOffset();
      case 0xf:
        return this.jmpProgramCounter();
    }
  }

  private jeqImmediate(): void {
    if (this.equalityFlag) this.jmpImmediate();
  }

  private jeqAbsolute(): void {
    if (this.equalityFlag) this.jmpAbsolute();
  }

  private jeqOffset(): void {
    if (this.equalityFlag) this.jmpOffset();
  }

  private jeqRegister(register: number): void {
    if (this.equalityFlag) this.jmpRegister(register);
  }

  private jeqRegisterAddress(register: number): void {
    if (this.equalityFlag) this.jmpRegisterAddress(register);
  }

  private jeqRegisterOffset(register: number): void {
    if (this.equalityFlag) this.jmpRegisterOffset(register);
  }

  private jeqStack(): void {
    if (this.equalityFlag) this.jmpStack();
  }

  private jeqStackAddress(): void {
    if (this.equalityFlag) this.jmpStackAddress();
  }

  private jeqStackOffset(): void {
    if (this.equalityFlag) this.jmpStackOffset();
  }

  private jeqProgramCounter(): void {
    if (this.equalityFlag) this.jmpProgramCounter();
  }

  private jeq(): void {
    switch (this.randomAccessMemory[this.programCounter++]) {
      case 0x0:
        return this.jeqImmediate();
      case 0x1:
        return this.jeqAbsolute();
      case 0x2:
        return this.jeqOffset();
      case 0x3:
        return this.jeqRegister(0);
      case 0x4:
        return this.jeqRegister(1);
      case 0x5:
        return this.jeqRegister(2);
      case 0x6:
        return this.jeqRegisterAddress(0);
      case 0x7:
        return this.jeqRegisterAddress(1);
      case 0x8:
        return this.jeqRegisterAddress(2);
      case 0x9:
        return this.jeqRegisterOffset(0);
      case 0xa:
        return this.jeqRegisterOffset(1);
      case 0xb:
        return this.jeqRegisterOffset(2);
      case 0xc:
        return this.jeqStack();
      case 0xd:
        return this.jeqStackAddress();
      case 0xe:
        return this.jeqStackOffset();
      case 0xf:
        return this.jeqProgramCounter();
    }
  }

  private jneImmediate(): void {
    if (!this.equalityFlag) this.jmpImmediate();
  }

  private jneAbsolute(): void {
    if (!this.equalityFlag) this.jmpAbsolute();
  }

  private jneOffset(): void {
    if (!this.equalityFlag) this.jmpOffset();
  }

  private jneRegister(register: number): void {
    if (!this.equalityFlag) this.jmpRegister(register);
  }

  private jneRegisterAddress(register: number): void {
    if (!this.equalityFlag) this.jmpRegisterAddress(register);
  }

  private jneRegisterOffset(register: number): void {
    if (!this.equalityFlag) this.jmpRegisterOffset(register);
  }

  private jneStack(): void {
    if (!this.equalityFlag) this.jmpStack();
  }

  private jneStackAddress(): void {
    if (!this.equalityFlag) this.jmpStackAddress();
  }

  private jneStackOffset(): void {
    if (!this.equalityFlag) this.jmpStackOffset();
  }

  private jneProgramCounter(): void {
    if (!this.equalityFlag) this.jmpProgramCounter();
  }

  private jne(): void {
    switch (this.randomAccessMemory[this.programCounter++]) {
      case 0x0:
        return this.jneImmediate();
      case 0x1:
        return this.jneAbsolute();
      case 0x2:
        return this.jneOffset();
      case 0x3:
        return this.jneRegister(0);
      case 0x4:
        return this.jneRegister(1);
      case 0x5:
        return this.jneRegister(2);
      case 0x6:
        return this.jneRegisterAddress(0);
      case 0x7:
        return this.jneRegisterAddress(1);
      case 0x8:
        return this.jneRegisterAddress(2);
      case 0x9:
        return this.jneRegisterOffset(0);
      case 0xa:
        return this.jneRegisterOffset(1);
      case 0xb:
        return this.jneRegisterOffset(2);
      case 0xc:
        return this.jneStack();
      case 0xd:
        return this.jneStackAddress();
      case 0xe:
        return this.jneStackOffset();
      case 0xf:
        return this.jneProgramCounter();
    }
  }

  private bslImmediate(): void {
    this.randomAccessMemory[this.programCounter++] <<= this.getNextOperand();
  }

  private bslAbsolute(): void {
    const addr = this.randomAccessMemory[this.programCounter++];
    this.randomAccessMemory[addr] <<= this.getNextOperand();
  }

  private bslOffset(): void {
    const addr = this.programCounter + this.randomAccessMemory[this.programCounter++];
    this.randomAccessMemory[addr] <<= this.getNextOperand();
  }

  private bslRegister(register: number): void {
    this.registers[register] <<= this.getNextOperand();
  }

  private bslRegisterAddress(register: number): void {
    const addr = this.registers[register];
    this.randomAccessMemory[addr] <<= this.getNextOperand();
  }

  private bslRegisterOffset(register: number): void {
    const addr = this.programCounter + this.registers[register];
    this.randomAccessMemory[addr] <<= this.getNextOperand();
  }

  private bslStack(): void {
    const value = this.stack.pop() << this.getNextOperand();
    this.stack.push(value);
  }

  private bslStackAddress(): void {
    const addr = this.stack.pop();
    this.randomAccessMemory[addr] <<= this.getNextOperand();
  }

  private bslStackOffset(): void {
    const addr = this.programCounter + this.stack.pop();
    this.randomAccessMemory[addr] <<= this.getNextOperand();
  }

  private bslProgramCounter(): void {
    this.programCounter <<= this.getNextOperand();
  }

  private bsl(): void {
    switch (this.randomAccessMemory[this.programCounter++]) {
      case 0x0:
        return this.bslImmediate();
      case 0x1:
        return this.bslAbsolute();
      case 0x2:
        return this.bslOffset();
      case 0x3:
        return this.bslRegister(0);
      case 0x4:
        return this.bslRegister(1);
      case 0x5:
        return this.bslRegister(2);
      case 0x6:
        return this.bslRegisterAddress(0);
      case 0x7:
        return this.bslRegisterAddress(1);
      case 0x8:
        return this.bslRegisterAddress(2);
      case 0x9:
        return this.bslRegisterOffset(0);
      case 0xa:
        return this.bslRegisterOffset(1);
      case 0xb:
        return this.bslRegisterOffset(2);
      case 0xc:
        return this.bslStack();
      case 0xd:
        return this.bslStackAddress();
      case 0xe:
        return this.bslStackOffset();
      case 0xf:
        return this.bslProgramCounter();
    }
  }

  private bsrImmediate(): void {
    this.randomAccessMemory[this.programCounter++] >>= this.getNextOperand();
  }

  private bsrAbsolute(): void {
    const addr = this.randomAccessMemory[this.programCounter++];
    this.randomAccessMemory[addr] >>= this.getNextOperand();
  }

  private bsrOffset(): void {
    const addr = this.programCounter + this.randomAccessMemory[this.programCounter++];
    this.randomAccessMemory[addr] >>= this.getNextOperand();
  }

  private bsrRegister(register: number): void {
    this.registers[register] >>= this.getNextOperand();
  }

  private bsrRegisterAddress(register: number): void {
    const addr = this.registers[register];
    this.randomAccessMemory[addr] >>= this.getNextOperand();
  }

  private bsrRegisterOffset(register: number): void {
    const addr = this.programCounter + this.registers[register];
    this.randomAccessMemory[addr] >>= this.getNextOperand();
  }

  private bsrStack(): void {
    const value = this.stack.pop() >> this.getNextOperand();
    this.stack.push(value);
  }

  private bsrStackAddress(): void {
    const addr = this.stack.pop();
    this.randomAccessMemory[addr] >>= this.getNextOperand();
  }

  private bsrStackOffset(): void {
    const addr = this.programCounter + this.stack.pop();
    this.randomAccessMemory[addr] >>= this.getNextOperand();
  }

  private bsrProgramCounter(): void {
    this.programCounter >>= this.getNextOperand();
  }

  private bsr(): void {
    switch (this.randomAccessMemory[this.programCounter++]) {
      case 0x0:
        return this.bsrImmediate();
      case 0x1:
        return this.bsrAbsolute();
      case 0x2:
        return this.bsrOffset();
      case 0x3:
        return this.bsrRegister(0);
      case 0x4:
        return this.bsrRegister(1);
      case 0x5:
        return this.bsrRegister(2);
      case 0x6:
        return this.bsrRegisterAddress(0);
      case 0x7:
        return this.bsrRegisterAddress(1);
      case 0x8:
        return this.bsrRegisterAddress(2);
      case 0x9:
        return this.bsrRegisterOffset(0);
      case 0xa:
        return this.bsrRegisterOffset(1);
      case 0xb:
        return this.bsrRegisterOffset(2);
      case 0xc:
        return this.bsrStack();
      case 0xd:
        return this.bsrStackAddress();
      case 0xe:
        return this.bsrStackOffset();
      case 0xf:
        return this.bsrProgramCounter();
    }
  }

  private addImmediate(): void {
    this.randomAccessMemory[this.programCounter++] += this.getNextOperand();
  }

  private addAbsolute(): void {
    const addr = this.randomAccessMemory[this.programCounter++];
    this.randomAccessMemory[addr] += this.getNextOperand();
  }

  private addOffset(): void {
    const addr = this.programCounter + this.randomAccessMemory[this.programCounter++];
    this.randomAccessMemory[addr] += this.getNextOperand();
  }

  private addRegister(register: number): void {
    this.registers[register] += this.getNextOperand();
  }

  private addRegisterAddress(register: number): void {
    const addr = this.registers[register];
    this.randomAccessMemory[addr] += this.getNextOperand();
  }

  private addRegisterOffset(register: number): void {
    const addr = this.programCounter + this.registers[register];
    this.randomAccessMemory[addr] += this.getNextOperand();
  }

  private addStack(): void {
    const value = this.stack.pop() + this.getNextOperand();
    this.stack.push(value);
  }

  private addStackAddress(): void {
    const addr = this.stack.pop();
    this.randomAccessMemory[addr] += this.getNextOperand();
  }

  private addStackOffset(): void {
    const addr = this.programCounter + this.stack.pop();
    this.randomAccessMemory[addr] += this.getNextOperand();
  }

  private addProgramCounter(): void {
    this.programCounter += this.getNextOperand();
  }

  private add(): void {
    switch (this.randomAccessMemory[this.programCounter++]) {
      case 0x0:
        return this.addImmediate();
      case 0x1:
        return this.addAbsolute();
      case 0x2:
        return this.addOffset();
      case 0x3:
        return this.addRegister(0);
      case 0x4:
        return this.addRegister(1);
      case 0x5:
        return this.addRegister(2);
      case 0x6:
        return this.addRegisterAddress(0);
      case 0x7:
        return this.addRegisterAddress(1);
      case 0x8:
        return this.addRegisterAddress(2);
      case 0x9:
        return this.addRegisterOffset(0);
      case 0xa:
        return this.addRegisterOffset(1);
      case 0xb:
        return this.addRegisterOffset(2);
      case 0xc:
        return this.addStack();
      case 0xd:
        return this.addStackAddress();
      case 0xe:
        return this.addStackOffset();
      case 0xf:
        return this.addProgramCounter();
    }
  }

  private hlt(): void {
    this.halted = true;
  }

  private execute(): void {
    switch (this.currentInstruction) {
      case 0x0:
        return;
      case 0x1:
        return this.mov();
      case 0x2:
        return this.inc();
      case 0x3:
        return this.dec();
      case 0x4:
        return this.and();
      case 0x5:
        return this.bor();
      case 0x6:
        return this.xor();
      case 0x7:
        return this.not();
      case 0x8:
        return this.teq();
      case 0x9:
        return this.jmp();
      case 0xa:
        return this.jeq();
      case 0xb:
        return this.jne();
      case 0xc:
        return this.bsl();
      case 0xd:
        return this.bsr();
      case 0xe:
        return this.add();
      case 0xf:
        return this.hlt();
    }
  }
}
