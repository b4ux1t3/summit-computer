import { MEMORY_SIZE } from './Constants';
import { Computer } from './Emulation/Computer';
import { parseRom } from './ParseRom';
import { configureUI } from './Setup';
import './style.css';

const programRom = document.querySelector('#program-code');
const loadButton = document.querySelector('#load-program');

export function onLoadClicked(): void {
  const rom = parseRom(programRom as HTMLTextAreaElement);
  const computer = new Computer(MEMORY_SIZE, 16, rom);
  configureUI(computer);
}

loadButton?.addEventListener('click', onLoadClicked);
