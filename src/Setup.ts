import { Computer } from './Emulation/Computer';
import { DebugFrame } from './Emulation/Types';

const debugDiv = document.querySelector('#debugger');
const registerDiv = document.querySelector('#registers');
const flagsDiv = document.querySelector('#flags');
const memoryDiv = document.querySelector('#memory');

export function configureUI(computer: Computer): void {
  const start = document.querySelector('#start') as HTMLInputElement;
  const cycle = document.querySelector('#cycle') as HTMLInputElement;
  const pause = document.querySelector('#pause') as HTMLInputElement;
  const halt = document.querySelector('#halt') as HTMLInputElement;
  const reset = document.querySelector('#reset') as HTMLInputElement;

  start.addEventListener('click', () => updateDebugger(computer.start()));
  cycle.addEventListener('click', () => updateDebugger(computer.cycle()));
  pause.addEventListener('click', () => console.log('Pause clicked'));
  halt.addEventListener('click', () => console.log('Halt clicked'));
  reset.addEventListener('click', () => console.log('Reset clicked'));
}

function debuggerDivReady(): boolean {
  return debugDiv !== null && registerDiv !== null && flagsDiv !== null && memoryDiv !== null;
}

function updateDebugger(frame: DebugFrame | null): void {
  if (!debuggerDivReady() || (frame == null)) return;
  if (registerDiv == null || flagsDiv == null || memoryDiv == null) return;

  registerDiv.innerHTML = '';
  flagsDiv.innerHTML = '';
  memoryDiv.innerHTML = '';

  populateRegisters(frame, registerDiv as HTMLDivElement);
  populateFlags(frame, flagsDiv as HTMLDivElement);
  populateMemory(frame, memoryDiv as HTMLDivElement);
}

function populateRegisters(frame: DebugFrame, registerDiv: HTMLDivElement): void {
  for (let i = 0; i < frame.registers.length; i++) {
    const registerValue = frame.registers[i];
    const newDiv = document.createElement('div');
    newDiv.innerHTML = `<p>Register ${i}: ${registerValue}</p>`;
    registerDiv.appendChild(newDiv);
  }
}

function populateStack(frame: DebugFrame, targetDiv: HTMLDivElement): void {
  const newList = document.createElement('ul');
  for (let i = 0; i < frame.stack.length; i++) {
    const newListItem = document.createElement('li');
    newListItem.innerText = `${i}: ${frame.stack[i]}`;
    newList.appendChild(newListItem);
  }
  const newHeader = document.createElement('h3');
  newHeader.innerText = 'Stack:';
  const newDiv = document.createElement('div');
  newDiv.appendChild(newHeader);
  newDiv.appendChild(newList);
  targetDiv.appendChild(newDiv);
}

function populateFlags(frame: DebugFrame, flagsDiv: HTMLDivElement): void {
  flagsDiv.innerHTML = `Equality Flag: ${frame.equalityFlag ? 'TRUE' : 'FALSE'}<br>Halted: ${frame.halted ? 'TRUE' : 'FALSE'}`;
}

function populateMemory(frame: DebugFrame, memoryDiv: HTMLDivElement): void {
  const newList = document.createElement('ul');
  for (let i = 0; i < frame.programList.length; i++) {
    const programListItem = frame.programList[i];
    const newListItem = document.createElement('li');
    newListItem.innerText = `0x${programListItem.lineNumber.toString(16)}: 0x${programListItem.value.toString(16)}`;
    if (programListItem.lineNumber === frame.programCounter) newListItem.id = 'current-line';
    newList.appendChild(newListItem);
  }
  const newHeader = document.createElement('h3');
  newHeader.innerText = 'Program:';
  const newDiv = document.createElement('div');
  newDiv.appendChild(newHeader);
  newDiv.appendChild(newList);
  memoryDiv.appendChild(newDiv);

  populateStack(frame, memoryDiv);
}
