const seperatorRegex = /\s+/;

export function parseRom(textArea: HTMLTextAreaElement): number[] {
  const tokens = textArea.value.split(seperatorRegex);
  return tokens.map(value => parseInt(value, 16));
}
